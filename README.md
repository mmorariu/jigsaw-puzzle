# Jigsaw Puzzle #

This repository contains my implementation for the jigsaw puzzle. Given an image as input, an `NxN` puzzle is generated from it, with `N` between 3 and 6, inclusively. The user can shuffle the pieces at any time, by clicking on `Shuffle` or can check the correct solution by clicking on `Solve`.

### How to play ###

* Open JigsawPuzzle.pro in QtCreator.
* Compile.
* Run.
* Have fun playing!